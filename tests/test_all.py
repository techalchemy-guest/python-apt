#!/usr/bin/python3
# Copyright (C) 2009 Julian Andres Klode <jak@debian.org>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.
"""Run all available unit tests."""
from __future__ import absolute_import, print_function
import glob
import os
import platform
import unittest.runner
import unittest
import sys
import sysconfig
from distutils.sysconfig import get_python_version
from collections import namedtuple


class ImplementationNamespace(
    namedtuple(
        "ImplementationNamespace", [
            "multiarch", "cache_tag", "hexversion", "name", "version"
        ]
    )
):
    @property
    def _multiarch(self):
        return self.multiarch


def get_namespace_tuple():
    interpreter_name = platform.python_implementation().lower()
    cache_tag = "{0}-{1}".format(
        interpreter_name,
        sysconfig.get_config_var("py_version_nodot")
    )
    return ImplementationNamespace(
        multiarch=sysconfig.get_config_var("MULTIARCH"),
        cache_tag=cache_tag,
        hexversion=sys.hexversion,
        name=interpreter_name,
        version=sys.version_info,
    )


# Python 3 only provides abiflags since 3.2
if getattr(sys, "pydebug", None) is None:
    sys.pydebug = bool(sysconfig.get_config_var("Py_DEBUG"))
if getattr(sys, "implementation", None) is None:
    sys.implementation = get_namespace_tuple()


def find_arch_dir(library_basedir):
    os_arch = platform.machine()
    if os_arch != "x86_64":
        return library_basedir % os_arch
    result = None
    for arch in [os_arch, "amd64"]:
        library_path = library_basedir % arch
        if os.path.exists(library_path):
            result = os.path.abspath(library_path)
            break
    return result


def get_package_from_dir(dir_, package):
    package_part = "{}.{}*-{}.so".format(
        package, sys.implementation.cache_tag,
        sys.implementation._multiarch
    )
    path = os.path.join(os.path.abspath(dir_), package_part)
    return next(iter(glob.glob(path)), None)


def get_library_dir():
    # Find the path to the built apt_pkg and apt_inst extensions
    python_minor_version = get_python_version()
    target_pkgs = ("apt_pkg", "apt_inst")
    if all(get_package_from_dir(os.getcwd(), pkg) for pkg in target_pkgs):
        return os.path.abspath(os.getcwd())
    os_platform = platform.system().lower().replace("/", "")
    if os.path.exists("../.pybuild"):
        debug_part = "{0}".format(sys.pydebug and "_dbg" or "")
        module_part = "{0}{1!s}_{2}{3}_python-apt".format(
            sys.implementation.name,
            sys.version_info[0],
            python_minor_version,
            debug_part,
        )
        module_path = os.path.abspath(os.path.join("../.pybuild", module_part))
        if os.path.exists(module_path):
            return module_path
    plat_specifier = ".%s-%%s-%s" % (os_platform, get_python_version())
    library_basedir = "../build/lib%s%s" % (plat_specifier,
                                            (sys.pydebug and "-pydebug" or ""))
    return find_arch_dir(library_basedir)


class MyTestRunner(unittest.runner.TextTestRunner):
    def __init__(self, *args, **kwargs):
        kwargs["stream"] = sys.stdout
        super(MyTestRunner, self).__init__(*args, **kwargs)


if __name__ == '__main__':
    if not os.access("/etc/apt/sources.list", os.R_OK):
        sys.stdout.write(
            "[tests] Skipping because sources.list is not readable\n")
        sys.exit(0)

    sys.stdout.write("[tests] Running on %s\n" % sys.version.replace("\n", ""))
    dirname = os.path.dirname(__file__)
    if dirname:
        os.chdir(dirname)
    library_dir = get_library_dir()
    sys.stdout.write("Using library_dir: '%s'" % library_dir)
    if library_dir:
        sys.path.insert(0, os.path.abspath(library_dir))

    for path in os.listdir('.'):
        if (path.endswith('.py') and os.path.isfile(path) and
            path.startswith("test_")):
            exec('from %s import *' % path[:-3])

    unittest.main(testRunner=MyTestRunner)
